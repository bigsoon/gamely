const webpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const path = require('path');

const config = require('./webpack.config.js');
const options = {
  contentBase: path.resolve(__dirname, 'src/assets'),
  compress: true,
  hot: true,
  host: 'localhost',
  historyApiFallback: true,
  proxy: {
    '/api': 'http://localhost:8181',
  },
};

webpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);
const server = new webpackDevServer(compiler, options);

server.listen(3001, 'localhost', () => {
  console.log('dev server listening on port 3001');
});
