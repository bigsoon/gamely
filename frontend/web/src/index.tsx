import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './App';

const root = document.getElementById('gamely');

ReactDOM.render(<App />, root);
