import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import 'phaser';

import { socket } from 'modules/socket-io';

import { Boot } from './scenes/Boot';
import { Game } from './scenes/Game';

class PongComp extends React.Component<RouteComponentProps> {
  public componentDidMount() {
    const {
      history,
      location: { search },
    } = this.props;

    const userRefreshedActiveGame = !search || search.slice(1) !== socket.id;

    if (userRefreshedActiveGame) {
      history.push('/');
    }

    socket.on('interruptGame', () => {
      history.push('/');
    });

    const config: GameConfig = {
      width: 800,
      height: 600,
      type: Phaser.AUTO,
      physics: {
        default: 'arcade',
        arcade: {
          // debug: true,
          // gravity: { y: 0 },
        },
      },
      scene: [Boot, Game],
      parent: 'game-pong',
    };

    new Phaser.Game(config);
  }

  public componentWillUnmount() {
    const { history } = this.props;

    window.removeEventListener('beforeunload', () => {
      history.push('/');
    });
  }

  public render() {
    return (
      <div
        id="container"
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          height: '100%',
          marginTop: '50px',
        }}
      >
        <div
          id="game-pong"
          style={{
            width: `${800}px`,
            height: `${600}px`,
          }}
        />
      </div>
    );
  }
}

export const Pong = withRouter(PongComp);
