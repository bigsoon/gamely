import { socket } from 'modules/socket-io';

import { Player } from '../objects/Player';
import { Ball } from '../objects/Ball';

export class Game extends Phaser.Scene {
  private cursors!: Phaser.Input.Keyboard.CursorKeys;
  private players: any = {};
  private ball!: Ball;
  private ballOldPosition: { x: number; y: number } = { x: 400, y: 300 };
  private ballVelocityX: number;
  private ballVelocityY: number;

  constructor() {
    super({
      key: 'Game',
    });

    this.ballVelocityX = -300;
    this.ballVelocityY = 300;
  }

  private gameOver() {
    this.ballVelocityX = -300;
    this.ballVelocityY = 300;
    socket.emit('ballMovement', { x: 400, y: 300 });
  }

  private hitPaddle() {
    // const offsetVelocityX = this.ballVelocityX > 0 ? 50 : -50;

    // this.ballVelocityX = (this.ballVelocityX + offsetVelocityX) * -1;
    this.ballVelocityX = this.ballVelocityX * -1;
    this.ball.setVelocityX(this.ballVelocityX * -1);

    if (this.ballVelocityY < 0) {
      this.ballVelocityY = this.ballVelocityY * -1;
      this.ball.setVelocityY(this.ballVelocityY);
    }
  }

  private createGameObjects(players: any, ball: any): void {
    this.cursors = this.input.keyboard.createCursorKeys();

    Object.keys(players).map(key => {
      const { id, x, y } = players[key];

      this.players[id] = new Player(this, x, y);
    });

    this.ball = new Ball(this, ball.x, ball.y);
    this.ball.setVelocityX(this.ballVelocityX);
    this.ball.setVelocityY(this.ballVelocityY);
  }

  private setCollisions(): void {
    Object.keys(this.players).map(key => {
      const player = this.players[key];

      this.physics.add.collider(
        this.ball,
        player,
        this.hitPaddle,
        undefined,
        this,
      );
    });
  }

  public create(): void {
    socket.emit('playerReady');
    socket.on('playersCreated', (players: any, ball: any) => {
      this.createGameObjects(players, ball);
      this.setCollisions();
    });

    // Listen for movement changes
    socket.on('playerMoved', ({ id, y }: any) => {
      const playersSize = Object.keys(this.players).length;

      if (playersSize > 0) {
        this.players[id].setY(y);
      }
    });
    socket.on('ballMoved', ({ x, y }: any) => {
      this.ball.setPosition(x, y);
    });
  }

  public update(): void {
    const playersSize = Object.keys(this.players).length;

    if (playersSize > 0) {
      const me = this.players[socket.id];
      if (this.cursors.up.isDown) {
        me.setVelocityY(-500);
      } else if (this.cursors.down.isDown) {
        me.setVelocityY(500);
      } else {
        me.setVelocityY(0);
      }
      socket.emit('playerMovement', { id: socket.id, y: me.y });

      if (
        this.ball.x !== this.ballOldPosition.x &&
        this.ball.y !== this.ballOldPosition.y
      ) {
        socket.emit('ballMovement', { x: this.ball.x, y: this.ball.y });
      }

      if (this.ball.x < 40 || this.ball.x > 760) {
        this.gameOver();
      }

      this.ballOldPosition = {
        x: this.ball.x,
        y: this.ball.y,
      };
    }
  }
}
