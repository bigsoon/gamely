export class Boot extends Phaser.Scene {
  constructor() {
    super({
      key: 'Boot',
    });
  }

  public preload() {
    this.load.image('paddle', '../pong/paddle.png');
    this.load.image('ball', '../pong/ball.png');
  }

  public create(): void {
    this.scene.start('Game');
  }
}
