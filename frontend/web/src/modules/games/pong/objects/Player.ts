import { Game } from '../scenes/Game';

export class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene: Game, x: number, y: number) {
    super(scene, x, y, 'paddle');

    scene.physics.world.enable(this);
    scene.add.existing(this);

    this.setCollideWorldBounds(true);
    this.setImmovable(true);
  }
}
