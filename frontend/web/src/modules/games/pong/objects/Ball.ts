import { Game } from '../scenes/Game';

export class Ball extends Phaser.Physics.Arcade.Sprite {
  constructor(scene: Game, x: number, y: number) {
    super(scene, x, y, 'ball');

    scene.physics.world.enable(this);
    scene.add.existing(this);

    this.setCollideWorldBounds(true);
    this.setBounce(1);
  }
}
