import React, { FunctionComponent, useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import deepOrange from '@material-ui/core/colors/deepOrange';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import purple from '@material-ui/core/colors/purple';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  withStyles,
  createStyles,
  WithStyles,
  Theme,
} from '@material-ui/core/styles';

import { socket } from 'modules/socket-io';

const styles = (theme: Theme) =>
  createStyles({
    avatar: {
      padding: 40,
      backgroundColor: deepOrange[500],
    },
    centered: {
      display: 'flex',
      justifyContent: 'center',
    },
    card: {
      marginTop: 100,
      minHeight: 300,
    },
    button: {
      marginTop: 50,
      width: 200,
      height: 75,
    },
    searchingText: {
      marginLeft: theme.spacing.unit,
    },
  });

interface IProps extends WithStyles<typeof styles> {}

const LobbyComp: FunctionComponent<IProps> = ({
  classes,
}: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false);
  const [toPong, setToPong] = useState<boolean>(false);

  const handleReady = () => {
    setLoading(true);
    socket.emit('findGame');
  };

  useEffect(() => {
    socket.on('startGame', (message: string) => {
      setTimeout(() => {
        setLoading(false);
        setToPong(true);
      }, 300);
    });
  }, []);

  if (toPong) {
    return <Redirect to={{ pathname: '/games/pong', search: socket.id }} />;
  }

  return (
    <Grid container>
      <Grid item xs={12} className={classes.centered}>
        <Card className={classes.card}>
          <CardContent>
            <Avatar className={classes.avatar}>
              <Typography>Morrigan</Typography>
            </Avatar>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} className={classes.centered}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleReady}
        >
          {loading ? (
            <React.Fragment>
              <CircularProgress style={{ color: purple[500] }} />
              <Typography className={classes.searchingText}>
                Searching...
              </Typography>
            </React.Fragment>
          ) : (
            'Ready'
          )}
        </Button>
      </Grid>
    </Grid>
  );
};

export const Lobby = withStyles(styles)(LobbyComp);
