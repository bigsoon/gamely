import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';

import { Lobby } from 'modules/lobby';
import { Pong } from 'modules/games/pong';

export class Routes extends React.Component {
  public render() {
    return (
      <Router>
        <Switch>
          <Route path="/games/pong" exact component={Pong} />
          <Route path="/" exact component={Lobby as any} />
          <Redirect to="/" />
        </Switch>
      </Router>
    );
  }
}
