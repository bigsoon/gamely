import { io } from './socket-server.mjs';
import { PLAYERS_CREATED } from './constants';

const players = {};
const ball = { x: 400, y: 300 };

export const playerReady = socket => {
  const playersSize = Object.keys(players).length;

  console.log('Players Size: ', playersSize);

  if (playersSize === 0) {
    const leftPlayer = {
      id: socket.id,
      x: 40,
      y: 300,
    };

    players[socket.id] = leftPlayer;
  }

  if (playersSize === 1) {
    const rightPlayer = {
      id: socket.id,
      x: 760,
      y: 300,
    };

    players[socket.id] = rightPlayer;

    Object.keys(socket.rooms).forEach(roomName => {
      if (roomName.includes('game-room-')) {
        // socket.broadcast.to(roomName).emit(PLAYERS_CREATED, players, ball);
        io.sockets.in(roomName).emit(PLAYERS_CREATED, players, ball);
      }
    });
  }
};

export const playerMovement = (socket, payload) => {
  Object.keys(socket.rooms).forEach(roomName => {
    if (roomName.includes('game-room-')) {
      socket.broadcast.to(roomName).emit('playerMoved', payload);
      // io.sockets.in(roomName).emit(PLAYERS_CREATED, players, ball);
    }
  });
};

export const ballMovement = (socket, payload) => {
  const firstSocket = Object.keys(players)[0];
  // Only one socket / player should send the ball position?
  if (firstSocket === socket.id) {
    console.log(socket.id);
    Object.keys(socket.rooms).forEach(roomName => {
      if (roomName.includes('game-room-')) {
        // socket.broadcast.to(roomName).emit('ballMoved', payload);
        io.sockets.in(roomName).emit('ballMoved', payload);
      }
    });
  }
};
