export const DISCONNECTING = 'disconnecting';
export const FIND_GAME = 'findGame';
export const READY_GAME = 'readyGame';
export const START_GAME = 'startGame';
export const INTERRUPT_GAME = 'interruptGame';
export const PLAYER_READY = 'playerReady';
export const PLAYERS_CREATED = 'playersCreated';
