import http from 'http';
import socketIo from 'socket.io';

import { app } from './app.mjs';

const server = http.Server(app);
const io = socketIo(server);

export { server, io };
