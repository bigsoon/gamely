import { io, server } from './socket-server.mjs';
import { findGame, interruptGame } from './room-events.mjs';
import {
  playerReady,
  playerMovement,
  ballMovement,
} from './pong-events.mjs';
import { DISCONNECTING, FIND_GAME, PLAYER_READY } from './constants.mjs';

io.sockets.on('connection', socket => {
  socket.on(DISCONNECTING, () => interruptGame(socket));
  socket.on(FIND_GAME, () => findGame(socket));
  socket.on(PLAYER_READY, () => playerReady(socket));
  socket.on('playerMovement', payload => playerMovement(socket, payload));
  socket.on('ballMovement', payload => ballMovement(socket, payload));
});

server.listen(8181, () => console.log('Listening on port 8181!'));
