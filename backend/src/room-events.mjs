import { io } from './socket-server.mjs';
import { START_GAME, INTERRUPT_GAME } from './constants.mjs';

let roomCounter = 1;
let currentRoom = 'game-room-1';

/**
 * Current implementation works only for duels (1v1).
 *  1. If there is a room with one player, other socket should join it.
 *     - emit startGame event to clients in that room
 *     - increase roomCounter by 1
 *     - adjust room variable for future rooms
 *  2. Else, create a new room by using socket.join(currentRoom)
 */
export const findGame = socket => {
  if (io.sockets.adapter.rooms[currentRoom]) {
    return socket.join(currentRoom, () => {
      io.sockets.in(currentRoom).emit(START_GAME);
      roomCounter++;
      currentRoom = `game-room-${roomCounter}`;
    });
  }

  return socket.join(currentRoom);
};

export const interruptGame = socket => {
  Object.keys(socket.rooms).forEach(roomName => {
    io.sockets.in(roomName).emit(INTERRUPT_GAME);
  });
};
